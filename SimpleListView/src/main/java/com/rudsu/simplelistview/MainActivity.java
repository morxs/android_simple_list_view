package com.rudsu.simplelistview;

import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
//import java.util.EventObject;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    private static long back_pressed;

    static String[] words = {"one", "two", "three"};
    static String[] descs = {"Lorem ipsum sit dolor amet satu",
            "Lorem ipsum sit dolor amet dua", "Lorem ipsum sit dolor amet tiga"};

//    HashMap<String, String> pairs = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init ListView widget
        final ListView listview = (ListView) findViewById(R.id.listview);
        // get values to array string
        String[] values = new String[] {"Android", "iPhone", "iDroid", "WinMo", "FirefoxOS", "Other",
                "Android1", "iPhone1", "iDroid1", "WinMo1", "FirefoxOS1", "Other1",
                "Android2", "iPhone2", "iDroid2", "WinMo2", "FirefoxOS2", "Other2",
                "Android3", "iPhone3", "iDroid3", "WinMo3", "FirefoxOS3", "Other3"};

        // Using StableArrayAdapter
        // push values array string to ArrayList
//        final ArrayList<String> list = new ArrayList<String>();
//        for (int i = 0; i < values.length; ++i) {
//            list.add(values[i]);
//        }
//
//        final StableArrayAdapter adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
//        listview.setAdapter(adapter);
//
//        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
////                Toast.makeText(getApplicationContext(), "Click ListItem number " + position, Toast.LENGTH_LONG);
//
//                final String item = (String) parent.getItemAtPosition(position);
//                view.animate().setDuration(700).alpha(0)
//                        .withEndAction(new Runnable() {
//                            @Override
//                            public void run() {
//                                list.remove(item);
//                                adapter.notifyDataSetChanged();
//                                view.setAlpha(1);
//                            }
//                        });
//            }
//        });
        // end of Using StableArrayAdapter

        // Using SimpleAdapter
//        pairs.put("Apple","iPhone");
//        pairs.put("Samsung","Galaxy");
//        pairs.put("Oppo","Find");

        final SimpleAdapter simpleAdapter = new SimpleAdapter();
        listview.setAdapter(simpleAdapter);
        // end of Using SimpleAdapter

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else
            Toast.makeText(getBaseContext(), R.string.back_again, Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {
        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public boolean hasStableIds() {
            return super.hasStableIds();
        }
    }

    private class SimpleAdapter extends BaseAdapter {
        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.list_simple, null);
            TextView name = (TextView) view.findViewById(R.id.text_name);
            name.setText(words[position]);
            TextView desc = (TextView) view.findViewById(R.id.text_desc);
            desc.setText(descs[position]);
            ImageView img = (ImageView) view.findViewById(R.id.img_logo);
            img.setImageResource(R.drawable.ic_launcher);
            return view;
        }

        @Override
        public Object getItem(int i) {
            return words[i];
//            return pairs[i];
        }

        @Override
        public int getCount() {
            return words.length;
        }
    }

    // TODO: Try to implement ArrayAdapter with custom XML layout
}
